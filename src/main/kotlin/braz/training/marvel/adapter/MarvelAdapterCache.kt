package braz.training.marvel.adapter

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.UNLIMITED
import java.util.Collections.synchronizedList

class MarvelAdapterCache(private val marvelAdapter: MarvelAdapter) {

    var charactersIds: MutableList<String> = synchronizedList(mutableListOf<String>())

    fun load() {

        val channel = Channel<List<String>>(UNLIMITED)

        for(i in 0 .. 1600 step 100){
            GlobalScope.launch {
                channel.send(marvelAdapter.getCharactersIds(offset = i))
            }
            GlobalScope.launch {
                charactersIds.addAll(channel.receive())
            }
        }



    }


}
