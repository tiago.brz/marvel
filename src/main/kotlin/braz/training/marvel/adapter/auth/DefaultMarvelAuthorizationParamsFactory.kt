package braz.training.marvel.adapter.auth

import java.math.BigInteger
import java.security.MessageDigest
import java.time.Instant

class DefaultMarvelAuthorizationParamsFactory(private val marvelIntegrationSettings: MarvelIntegrationSettings) :
    MarvelAuthorizationParamsFactory {

    override fun create(): Map<String, String> {
        val timestamp = Instant.now().toEpochMilli()
        return mapOf(
            "ts" to timestamp.toString(),
            "hash" to createHash(timestamp),
            "apikey" to marvelIntegrationSettings.apiPublicKey
        )
    }

    private fun createHash(timestamp: Long): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(
            1,
            md.digest("$timestamp${marvelIntegrationSettings.apiPrivateKey}${marvelIntegrationSettings.apiPublicKey}".toByteArray())
        ).toString(16).padStart(32, '0')
    }


}