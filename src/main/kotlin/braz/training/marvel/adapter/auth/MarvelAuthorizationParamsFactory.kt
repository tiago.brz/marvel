package braz.training.marvel.adapter.auth

interface MarvelAuthorizationParamsFactory {

    fun create() : Map<String, String>

}