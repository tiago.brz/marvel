package braz.training.marvel.adapter.auth

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "integrations.marvel")
data class MarvelIntegrationSettings (val apiPublicKey : String, val apiPrivateKey : String, val baseUri : String)