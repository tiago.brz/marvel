package braz.training.marvel.adapter.auth

import org.springframework.http.HttpRequest
import org.springframework.http.client.ClientHttpRequestExecution
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.http.client.ClientHttpResponse
import org.springframework.http.client.support.HttpRequestWrapper
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI
import java.security.PrivateKey


class MarvelAuthorizationHashInterceptor(private val paramsFactory: MarvelAuthorizationParamsFactory) :
    ClientHttpRequestInterceptor {

    override fun intercept(
        request: HttpRequest,
        body: ByteArray,
        execution: ClientHttpRequestExecution
    ): ClientHttpResponse {

        val uri = UriComponentsBuilder.fromHttpRequest(request)
            .apply {
                paramsFactory.create().entries.forEach {
                    queryParam(it.key, it.value)
                }
            }.build().toUri()

        return execution.execute(InjectQueryParameterClientHttpRequest(request, uri), body)
    }

    class InjectQueryParameterClientHttpRequest(httpRequest: HttpRequest,private val uri: URI) : HttpRequestWrapper(httpRequest) {
        override fun getURI(): URI {
            return uri
        }
    }

}



