package braz.training.marvel.adapter

import braz.training.marvel.adapter.dto.CharacterDto

interface MarvelAdapter {

    fun getCharactersIds(offset : Int = 0, limit : Int = 100): List<String>

    fun getCharacter(id : String): CharacterDto

}
