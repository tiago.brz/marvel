package braz.training.marvel.adapter

import braz.training.marvel.adapter.dto.CharacterDto
import com.jayway.jsonpath.JsonPath
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder


class DefaultMarvelAdapter(private val restTemplate: RestTemplate, private val baseUri: String) : MarvelAdapter {

    override fun getCharactersIds(offset: Int, limit: Int): List<String> {

        val uri = UriComponentsBuilder.fromHttpUrl("$baseUri/characters")
            .queryParam("limit", limit)
            .queryParam("offset", offset)
            .build().toUri()

        return restTemplate.getForEntity(uri, String::class.java)
            .let {
                JsonPath.parse(it.body).read<List<Int>>("\$.data.results[*].id")
                    .map { id -> id.toString() }
            }
    }

    override fun getCharacter(id: String): CharacterDto {
        return restTemplate.getForEntity("$baseUri/characters/$id", String::class.java)
            .let {
                val parse = JsonPath.parse(it.body)
                CharacterDto(
                    parse.read<Int>("\$.data.results[0].id").toString(),
                    parse.read("\$.data.results[0].name"),
                    parse.read("\$.data.results[0].description"),
                    parse.read("\$.data.results[0].thumbnail.path"),
                    parse.read("\$.data.results[0].thumbnail.extension"),
                )
            }
    }
}