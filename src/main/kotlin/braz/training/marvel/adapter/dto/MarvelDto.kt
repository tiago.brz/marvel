package braz.training.marvel.adapter.dto

data class CharacterDto(val id: String, val name: String, val description: String, val thumbnailPath: String, val thumbnailExtension: String)