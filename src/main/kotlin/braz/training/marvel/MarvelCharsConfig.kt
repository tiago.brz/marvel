package braz.training.marvel

import braz.training.marvel.adapter.DefaultMarvelAdapter
import braz.training.marvel.adapter.MarvelAdapter
import braz.training.marvel.adapter.MarvelAdapterCache
import braz.training.marvel.adapter.auth.DefaultMarvelAuthorizationParamsFactory
import braz.training.marvel.adapter.auth.MarvelAuthorizationHashInterceptor
import braz.training.marvel.adapter.auth.MarvelIntegrationSettings
import braz.training.marvel.domain.CharactersRepository
import braz.training.marvel.domain.DefaultCharactersRepository
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate


@Configuration
@EnableConfigurationProperties(MarvelIntegrationSettings::class)
class MarvelCharsConfig {

    @Bean
    fun restTemplate(marvelIntegrationSettings: MarvelIntegrationSettings) =
        RestTemplate().apply {
            interceptors.add(
                MarvelAuthorizationHashInterceptor(
                    DefaultMarvelAuthorizationParamsFactory(
                        marvelIntegrationSettings
                    )
                )
            )
        }

    @Bean
    fun marvelAdapter(restTemplate: RestTemplate, marvelIntegrationSettings: MarvelIntegrationSettings) =
        DefaultMarvelAdapter(restTemplate, marvelIntegrationSettings.baseUri)

    @Bean
    fun marvelAdapterCache(marvelAdapter: MarvelAdapter): MarvelAdapterCache {
        return MarvelAdapterCache(marvelAdapter)
            .apply { load() }
    }

    @Bean
    fun charactersRepository(
        restTemplate: RestTemplate,
        marvelIntegrationSettings: MarvelIntegrationSettings,
        marvelAdapterCache: MarvelAdapterCache,
        marvelAdapter: MarvelAdapter
    ): CharactersRepository {
        return DefaultCharactersRepository(
            marvelAdapter,
            marvelAdapterCache
        )
    }


}