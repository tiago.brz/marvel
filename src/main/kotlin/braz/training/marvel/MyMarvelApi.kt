package braz.training.marvel

import braz.training.marvel.domain.Character
import braz.training.marvel.domain.CharactersRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class MyMarvelApi(val charactersRepository: CharactersRepository) {

    @GetMapping("/characters")
    fun getCharacters(): List<String> {
        return charactersRepository.getCharactersIds()
    }

    @GetMapping("/characters/{id}")
    fun getCharacter(@PathVariable("id") id : String): Character {
        return charactersRepository.getCharacter(id)
    }

}