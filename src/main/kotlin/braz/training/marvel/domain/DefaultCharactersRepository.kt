package braz.training.marvel.domain

import braz.training.marvel.adapter.MarvelAdapter
import braz.training.marvel.adapter.MarvelAdapterCache

class DefaultCharactersRepository(
    private val marvelAdapter: MarvelAdapter,
    private val marvelAdapterCache: MarvelAdapterCache
) : CharactersRepository {

    override fun getCharactersIds(): List<String> {
        return marvelAdapterCache.charactersIds
    }

    override fun getCharacter(id: String): Character {
        val characterDto = marvelAdapter.getCharacter(id)
        return Character(
            characterDto.id,
            characterDto.name,
            characterDto.description,
            "${characterDto.thumbnailPath}.${characterDto.thumbnailExtension}"
        )
    }
}