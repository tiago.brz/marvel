package braz.training.marvel.domain

data class Character(val id: String, val name: String, val description: String, val thumbnail: String)