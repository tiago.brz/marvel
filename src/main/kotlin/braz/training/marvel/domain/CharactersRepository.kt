package braz.training.marvel.domain

interface CharactersRepository {

    fun getCharactersIds(): List<String>

    fun getCharacter(id : String): Character

}