package braz.training.marvel.adapter.auth

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.math.BigInteger
import java.security.MessageDigest
import java.security.Timestamp

internal class DefaultMarvelAuthorizationParamsFactoryMyMarvelApi {

    @Test
    fun hashValidation() {
        val input = "1abcd1234"
        val md = MessageDigest.getInstance("MD5")
        assertEquals(
            BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0'),
            "ffd275c5130566a2916217b101f26150"
        )
    }
}